/* user and group to drop privileges to */
static const char *user  = "kishore";
static const char *group = "kishore";

static const char *colorname[NUMCOLS] = {
	[INIT] =   "black",     /* after initialization */
	[INPUT] =  "#005577",   /* during input */
	[FAILED] = "#C22020",   /* wrong password */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;
